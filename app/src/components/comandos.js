export default {
    execCmd(areaEditor, command) {
        areaEditor.document.execCommand(command, false, null)
    },
    execCommandWithArg(areaEditor, command, arg) {
        areaEditor.document.execCommand(command, false, arg)
    },
    desabilitarModoEdicao(areaEditor) {
        areaEditor.document.designMode = 'Off'
    },
    habilitarModoEdicao(areaEditor) {
        areaEditor.document.designMode = 'On'
    }
}