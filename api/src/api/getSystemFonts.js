const fontFinder = require('font-finder');

const getFonts = async (req, res) => {
    const fontes = await fontFinder.list()
    return res.send(fontes)
}

module.exports = { getFonts }

