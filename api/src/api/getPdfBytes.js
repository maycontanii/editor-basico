const pdf = require('html-pdf');

const getByte = (req, res) => {
    const html = req.body.file.html;
    const options = {
        format: 'Letter',
        border: {
            top: "0.5in",
            right: "0.5in",
            bottom: "0.5in",
            left: "0.5in"
        }
    };

    pdf.create(html, options).toBuffer(function (error, buffer) {
        if (error) return res.json({
            status: 400,
            data: error
        });
        res.set({
            'Content-Type': 'application/pdf'
        });
        const base64 = buffer.toString('base64');
        res.send(base64)
    })
};

module.exports = {getByte};