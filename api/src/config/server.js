const express = require('express');
const bodyParser = require('body-parser');
const allowCors = require('./cors');

const server = express();

server.use(bodyParser.urlencoded({extended: true}));
server.use(bodyParser.json());
server.use(allowCors);

server.listen(process.env.PORT || 3000, function () {
    console.log("Express server listening on port %d in %s mode", this.address().port, server.settings.env)
});

module.exports = server;