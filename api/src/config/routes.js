const express = require('express');
const fontes = require('../api/getSystemFonts');
const pdf = require('../api/getPdfBytes');

const api = express.Router();

module.exports = function (server) {
    server.use('/api', api);

    api.get('/fontes', fontes.getFonts);
    api.post('/pdf', pdf.getByte)
};